---
layout: '~/layouts/Markdown.astro'
title: 'Forgejo v1.20 documentation'
---

- [What is Forgejo?](https://forgejo.org/)
- [Installation](https://forgejo.org/download/)
- [FAQ](https://forgejo.org/faq/)
- [Administrator guide](admin)
- [User guide](user)
- [Developer guide](developer)
- [License](license)
