---
layout: '~/layouts/Markdown.astro'
title: 'Forgejo developer guide'
---

This area will be targeted to people who want to hack Forgejo and adjust it to
their needs.

- [code.forgejo.org](code-forgejo-org)
