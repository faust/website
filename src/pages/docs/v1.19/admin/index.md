---
layout: '~/layouts/Markdown.astro'
title: 'Forgejo v1.19 administrator guide'
---

These documents are targeted to people who run Forgejo on their machines.

- [Seek Assistance](seek-assistance)
- [Database Preparation](database-preparation)
- [Configuration Cheat Sheet](config-cheat-sheet)
- [Upgrade guide](upgrade)
- [Command Line](command-line)
- [Reverse Proxy](reverse-proxy)
- [Email setup](email-setup)
- [Incoming Email](incoming-email)
- [Logging Configuration](logging-documentation)
