---
layout: '~/layouts/Markdown.astro'
publishDate: 'Aug 08 2022'
title: 'Forgejo FAQ'
---

## Where does the name come from?

**Forgejo** ([pronounced /forˈd͡ʒe.jo/](/static/forgejo.mp4)) is inspired by <i lang="eo">forĝejo</i>, the Esperanto word for forge.

## Why fork Gitea?

In October 2022 the domains and trademark of Gitea were transferred to a for-profit company without knowledge or approval of the community. Despite [writing an open letter](https://gitea-open-letter.coding.social/), the takeover was later confirmed. The goal of Forgejo is to continue developing the code with a healthy democratic governance.

## How is it different from Gitea?

Gitea is [controlled by the for profit company Gitea Ltd](https://codeberg.org/SocialCoding/gitea-open-letter). Forgejo is a [community led](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/GOVERNANCE.md) soft fork of Gitea with [additional features](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md#feature-branches).

## Does Forgejo compete with Gitea?

No. Forgejo is a soft fork of Gitea, in a way that is similar to [LineageOS](https://lineageos.org/) a community led distribution based on Android from Google. It intends to reunite the Gitea community: all Gitea contributors are welcome in Forgejo and Forgejo contributor can also participate in Gitea.

## Are migrations between Gitea and Forgejo possible?

Yes, because for now Forgejo is a "soft fork". This means that all commits made on [Gitea](https://github.com/go-gitea/gitea/) are also present in Forgejo. Some additional modifications are made, to improve scaling, federation and privacy.

## Using Free Software to develop Free Software

Forgejo development happens on [Codeberg](https://codeberg.org), which is itself running Forgejo and [Woodpecker](https://woodpecker-ci.org/) (for continuous integration). By "dogfooding" our code, we aim at being close to our users and serve them better.

## Who owns the Forgejo domains and trademarks?

The Forgejo domains are in the custody of [Codeberg e.V.](https://docs.codeberg.org/getting-started/what-is-codeberg/#what-is-codeberg-e.v.%3F) a non-profit based in Germany and dedicated to [hosting Free Software projects](https://codeberg.org/) since 2019. It is ultimately in control of Forgejo and [its bylaws](https://codeberg.org/Codeberg/org/src/branch/main/en/bylaws.md) guarantee it will keep further the interest of the general public. No trademark was registered at this point in time.

## What is 'Codeberg e.V.'?

[Codeberg e.V.](https://codeberg.org/Codeberg/org/src/branch/main/Imprint.md) is an association registered in Berlin, Germany. The **e.V.** abbreviation is for _eingetragener Verein_, which translates as 'registered association'. The link above is to the Impressum, which includes the required contact information, statements regarding non-profit status, and other helpful information. You may verify this information via the [German Federal Registration portal](https://www.handelsregister.de/rp_web/normalesuche.xhtml); just type in `Codeberg e.V.` as the company, click the Find button, and select the SI link for an xml structured report.

## Is Forgejo sustainable? How is it funded?

Sustaining Free Software projects developed in the interest of the general public is an ongoing challenge. Forgejo relies on a mixture of volunteer contributions, grants, donations and employee delegation to keep going. The details are documented transparently [in a repository dedicated to sustainability](https://codeberg.org/forgejo/sustainability).

## Is there a roadmap for Forgejo?

There is just one item on the roadmap: forge federation. [User research](https://jdittrich.github.io/userNeedResearchBook/) is ongoing to build a roadmap that reflects actual user needs. If you are interested to participate, please [join the chat room](https://matrix.to/#/#forgejo-chat:matrix.org).

## Who is using Forgejo?

[Codeberg.org](https://codeberg.org) has been using Forgejo starting with version 1.18.0 to host over 50,000 projects and 40,000 users. Various organizations and individuals will also upgrade from Gitea [to Forgejo](/download/) simply by changing the URL from which they download releases.

## What is the governance of Forgejo?

Forgejo is a collective of individuals who [define their own governance](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/GOVERNANCE.md). The first meeting happened [on October 24th, 2022](https://codeberg.org/forgejo/meta/issues/19#issuecomment-694460) and everyone is [welcome to participate](https://codeberg.org/forgejo/meta/issues/19). Codeberg e.V. does not run the day to day operations of Forgejo nor does it require the members of the project to follow its governance. It is solely responsible for ensuring the domains and trademarks are ultimately used to further the interest of the general public in accordance to its bylaws.

## Does Forgejo have a Code of Conduct?

Yes. The [Code of Conduct](https://codeberg.org/forgejo/code-of-conduct) applies to all spaces that are under the responsibility of the Forgejo project. The [Well Being and Moderation](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/COC.md) teams are available to avoid tension and resolve conflicts.

## Is Forgejo licensed under AGPL?

No. The [license of Forgejo](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/LICENSE) is the same as the license of Gitea. It was suggested to change the license to AGPL in order to prevent a takeover from a for-profit company. It was however decided that the best way to prevent such a takeover was to have the Forgejo domains and trademarks in the custody of the Codeberg e.V. non-profit.

## How are security vulnerabilities handled?

Security issues are managed by [a team](https://forgejo.org/.well-known/security.txt) sharing the effort between Codeberg and Forgejo.
The security team is available at `security@forgejo.org` (GPG public key [1B638BDF10969D627926B8D9F585D0F99E1FB56F](https://keyoxide.org/security@forgejo.org)) and is tasked to identify, define, and respond to vulnerabilities.

## Why are container images published with the 1.19 tag?

The **1.19** tag is set to be the latest patch release, starting with [1.19.0-2](https://codeberg.org/forgejo/-/packages/container/forgejo/1.19.0-2). **1.19** will then be equal to **1.19.1-0** when it is released and so on. It can conveniently be used for automated upgrades to the latest stable release.

## Why is there no latest tag for container images?

Because upgrading from **1.X** to **1.X+1** (for instance from **1.18** to **1.19**) requires a [manual operation and human verification](https://forgejo.org/docs/latest/admin/upgrade/). However it is possible to use the **X.Y** tag (for instance **1.19**) to get the latest point release automatically.

## Why must I keep the binary name `gitea` on upgrade?

Because the `gitea` binary file name is referenced by an existing Gitea installation
and would need to be replaced if Forgejo was installed as `forgejo` instead.
It is the case, for instance, within the `git` hooks.

Using a symbolic link from `gitea` to `forgejo` makes it simple and convenient to use both
names while preserving backward compatibility.

## What are the names of the built-in Forgejo themes?

Forgejo introduces two new themes: light version named **forgejo-light** and a dark version named **forgejo-dark**. They are the default for a new installation but will need to be set explicitly if the `app.ini` file already has a custom list of themes. For instance, if it looks like this:

```
[ui]
THEMES = auto,arc-green,gitea
```

the **forgejo-auto**, **forgejo-light** and **forgejo-dark** can be added as follows:

```
[ui]
THEMES = forgejo-auto,forgejo-light,forgejo-dark,auto,arc-green,gitea
```

## Is Woodpecker integrated with Forgejo?

[Woodpecker CI](https://woodpecker-ci.org/) is compatible with Forgejo but requires [a separate installation](https://woodpecker-ci.org/docs/next/administration/setup).
It is not integrated or distributed with Forgejo.
