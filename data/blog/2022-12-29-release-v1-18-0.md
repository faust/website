---
title: Forgejo v1.18 stable is released
publishDate: 2022-12-29
tags: ['releases']
excerpt: The first stable release of Forgejo was published, based on Gitea v1.18.0.
---

Today [Forgejo v1.18.0-1](https://codeberg.org/forgejo/forgejo/releases/tag/v1.18.0-1) was released.
This is the first stable release of Forgejo, and is based on
[Gitea v1.18.0](https://blog.gitea.io/2022/12/gitea-1.18.0-is-released/)
which was also released today.

### Get Forgejo v1.18

See the [download page](/download)
for instructions on how to install Forgejo, and read the
[release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-18-0-1)
for more information on what's new.

#### Upgrading from Gitea

This is the moment you have been waiting for. This stable release offers an upgrade and a transition path from Gitea to Forgejo.
If you're on `v1.17.x` or older please read the
[release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-18-0-1)
carefully, and in particular check out the
[breaking changes](https://blog.gitea.io/2022/12/gitea-1.18.0-is-released/#breaking-changes)
section of Gitea's blog post.

The actual upgrade process is as simple as replacing the Gitea binary or container image
with the corresponding [Forgejo binary](https://codeberg.org/forgejo/forgejo/releases/tag/v1.18.0-1)
or [container image](https://codeberg.org/forgejo/-/packages/container/forgejo/1.18.0-1).
If you're using the container images, you can use the
[`1.18` tag](https://codeberg.org/forgejo/-/packages/container/forgejo/1.18)
to stay up to date with the latest `1.18.x` point release automatically.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo, we'd love to hear from you!
Open an issue on [our issue tracker](https://codeberg.org/forgejo/forgejo/issues)
for feature requests or bug reports, find us [on the Fediverse](https://floss.social/@forgejo),
or drop into [our Matrix space](https://matrix.to/#/#forgejo:matrix.org)
([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) and say hi!
